# sriracha
[![GoDoc](https://gitlab.com/tslocum/godoc-static/-/raw/master/badge.svg)](https://docs.rocketnine.space/gitlab.com/tslocum/sriracha)
[![CI status](https://gitlab.com/tslocum/sriracha/badges/master/pipeline.svg)](https://gitlab.com/tslocum/sriracha/commits/master)
[![Donate](https://img.shields.io/liberapay/receives/rocketnine.space.svg?logo=liberapay)](https://liberapay.com/rocketnine.space)

Image board

## Features

- [**Extensions**](https://gitlab.com/tslocum/sriracha/blob/master/EXTENSIONS.md)
  - Process user posts
  - Create automatic posts
  - Provide support for additional filetypes

## Demo

*Coming soon*

## Install

```bash
go get gitlab.com/tslocum/sriracha/cmd/sriracha
```

## Configure

See [CONFIGURATION.md](https://gitlab.com/tslocum/sriracha/blob/master/CONFIGURATION.md)

## Extend

See [EXTENSIONS.md](https://gitlab.com/tslocum/sriracha/blob/master/EXTENSIONS.md)

## Run

```bash
sriracha
```

## Support

Please share issues and suggestions [here](https://gitlab.com/tslocum/sriracha/issues).
