package sriracha

import "time"

type Attachment struct {
	ID   int
	Post int

	File string
	Size int64
	Hash string

	Thumb       string
	ThumbWidth  int
	ThumbHeight int

	Uploaded time.Time
}
