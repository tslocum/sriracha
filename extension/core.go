package extension

import (
	"io"

	"gitlab.com/tslocum/sriracha"
)

type Core struct {
}

func NewCore() *Core {
	core := &Core{}

	return core
}

func (c *Core) Start() error {
	return nil
}

func (c *Core) Attach(file io.Reader, size int64, mime string) (*sriracha.Attachment, error) {
	return nil, nil
}

func (c *Core) Post() error {
	return nil
}

func (c *Core) Stop() error {
	return nil
}
