package sriracha

import (
	"errors"
	"fmt"
	"io"
)

type UseExtension struct {
	Extension Extension
	FileTypes []string
	Events    []string
}

type Options struct {
	Extensions []UseExtension
}

// sriracha represents an imageboard system.
type sriracha struct {
	boards     map[string]*Board
	extensions []UseExtension

	newPost chan *Post
}

func (s *sriracha) handleAttachment(file io.ReadSeeker, size int64, mime string) (*Attachment, error) {
	if mime == "" {
		return nil, errors.New("unsupported file type")
	}

	for _, ext := range s.extensions {
		for _, fileType := range ext.FileTypes {
			if fileType == mime {
				_, err := file.Seek(0, io.SeekStart)
				if err != nil {
					return nil, errors.New("failed to seek to start of file")
				}

				attachment, err := ext.Extension.Attach(file, size, mime)
				if err != nil {
					return nil, err
				} else if attachment != nil {
					return attachment, nil
				}
			}
		}
	}

	return nil, fmt.Errorf("unsupported file type: %s", mime)
}

func (s *sriracha) handlePosts() {
	for post := range s.newPost {
		for _, ext := range s.extensions {
			ext.Extension.Post() // TODO
			_ = post
		}
	}
}

// Run initializes and runs a sriracha imageboard system.
func Run(options *Options) error {
	s := &sriracha{
		boards:     make(map[string]*Board),
		extensions: options.Extensions,
		newPost:    make(chan *Post, 10),
	}

	done := make(chan error)
	go func() {
		done <- s.runWebServer()
	}()

	go s.handlePosts()

	return <-done
}
