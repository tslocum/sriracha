package sriracha

// User represents a user account.
type User struct {
	ID       int
	Access   int
	Email    string
	Password string
	Name     string
}
