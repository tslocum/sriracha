package sriracha

import (
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/h2non/filetype"
)

const indexPage = `
<form method="post" action="/upload" enctype="multipart/form-data">
<input type="file" name="file"><input type="submit">
</form>
`

func (s *sriracha) handleIndex(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")

	fmt.Fprintf(w, indexPage)
}

func (s *sriracha) uploadFile(w http.ResponseWriter, r *http.Request) {
	r.ParseMultipartForm(10 << 20)

	file, header, err := r.FormFile("file")
	if err != nil {
		log.Println("failed to get uploaded file")
		return
	}
	defer file.Close()

	fileTypeResult, err := filetype.MatchReader(file)
	if err != nil || fileTypeResult.MIME.Value == "" {
		log.Fatal("failed to get file type")
	}
	mime := strings.ToLower(strings.TrimSpace(fileTypeResult.MIME.Value))

	attachment, err := s.handleAttachment(file, header.Size, mime)
	if err != nil {
		log.Fatalf("failed to attach file: %s", err)
	}

	log.Println(attachment)
}

func (s *sriracha) runWebServer() error {
	http.HandleFunc("/", s.handleIndex)
	http.HandleFunc("/upload", s.uploadFile)

	return http.ListenAndServe(":8080", nil)
}
