package sriracha

import "io"

// Extension defines the methods of sriracha extensions.
type Extension interface {
	Start() error // Initialize

	Stop() error // Terminate

	Attach(file io.Reader, size int64, mime string) (*Attachment, error) // Attach a file

	Post() error // Process a post
}
