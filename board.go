package sriracha

// Board types
const (
	BoardNormal   = 1
	BoardCategory = 2
	BoardLink     = 3
)

// Board represents an imageboard, category or link.
type Board struct {
	ID          int
	Parent      int
	Type        int
	Dir         string
	Name        string
	Description string
	MinAccess   int
}
