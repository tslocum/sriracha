package sriracha

import "time"

type Post struct {
	ID      int
	Parent  int
	Subject string
	Name    string
	Message string

	Posted   time.Time
	Modified time.Time
	Bumped   time.Time
}
