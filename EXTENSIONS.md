WIP

# Creating an extension

Extensions which handle uploads accept a file and associated size and MIME
type, and return an attachment and an error.

When passed an unhandled file type, return nil for both.

# Using an extension

When registering an extension featuring attachment handlers, each MIME type
which should be handled by that extension must be listed.

If you installed `sriracha` via `go get`, create a new folder where we will
define our custom sriracha build and add a new file named `main.go` containing:

```go
package main

import (
	"log"

	"gitlab.com/tslocum/sriracha"
	"gitlab.com/tslocum/sriracha/extension"
	
	"gitlab.com/awesomeuser/awesomeextensions"
)

func main() {
	// Initialize third-party extensions
	fooExtension := awesomeextensions.NewFoo()
	barExtension := awesomeextensions.NewBar()

	// Initialize core extension
	core := extension.NewCore()
	
	options := &sriracha.SrirachaOptions{
		Extensions: []sriracha.UseExtension{
			{Extension: fooExtension, FileTypes: []string{"application/doc", "application/docx", "application/pdf"}},
			{Extension: barExtension, FileTypes: []string{"application/x-tar", "application/zip"}},
			{Extension: core, FileTypes: []string{"image/gif", "image/jpeg", "image/png"}},
		},
	}

	err := sriracha.Run(options)
	if err != nil {
		log.Fatal(err)
	}
}
```

Remove the `sriracha` binary installed via go get:

```bash
rm ~/go/bin/sriracha
```

Build and run the customized `sriracha` binary:

```bash
go mod init
go build
./sriracha
```

To update:

```bash
go get ./...
go build
./sriracha
```